La procédure est fournie dans la documentation : [https://docs.docker.com/compose/install/](https://docs.docker.com/compose/install/).

Pour résumer :

```
COMPOSE_VERSION=1.29.2
export HTTP_PROXY=http://10.0.4.2:3128 # Proxy ENSG
sudo -E curl -L https://github.com/docker/compose/releases/download/${COMPOSE_VERSION}/docker-compose-`uname -s`-`uname -m` -o /usr/local/bin/docker-compose
sudo -E curl -L https://raw.githubusercontent.com/docker/compose/${COMPOSE_VERSION}/contrib/completion/bash/docker-compose -o /etc/bash_completion.d/docker-compose
sudo chmod +x /usr/local/bin/docker-compose
```


Pour vérifier l'installation :
```
docker-compose --version
```
