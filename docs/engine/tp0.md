
*Nous allons prendre en main Docker : ses concepts et son interface CLI.*

## Hello-world

### Testons votre installation

```shell
docker container run hello-world
```

La commande par défaut de cette image affiche un message confirmant la bonne installation de *Docker Engine*.
Si l'image n'est pas disponible en local, elle sera téléchargée sans avoir à faire un `docker image pull hello-world`

### Gérer les conteneurs

- Observer que le conteneur est bien existant et à l'arrêt

    ```shell
    docker container ls
    docker container ls -a
    ```

    ??? note "Correction"
        La première commande n'affiche rien car le conteneur lancé est déjà arrêté. En effet, l'image `hello-world` lance une commande qui affiche un message et c'est tout. Il faut ajouter l'option `-a` pour afficher également les conteneurs arrêtés.

- Supprimer le

    ```shell
    docker container rm NAME
    ```

    ??? note "Correction"
        On peut trouver le nom du conteneur à supprimer dans le résultat de la commande `docker container ls -a`. Si aucun nom n'est spécifié lors de la création du conteneur (option `--name`), docker génère un nom alléatoire composé d'un adjectif et d'un nom de personnalité. 

        On pourrait aussi utiliser l'identifiant du conteneur à la place du nom.

- Le conteneur n'existe plus, et l'image ?

    ??? note "Correction"
        La commande `docker container rm` ne supprime que le conteneur. Pour supprimer l'image, il faut utiliser la commande `docker image rm`.

        Une image ne peut pas être supprimée si il existe un conteneur basé dessus, même stoppé.


### Les images

- Afficher la liste des images

    ??? note "Correction"
        ```shell
        docker image ls
        ```

- supprimer l'image `hello-world`

    ??? note "Correction"
        ```shell
        docker image rm hello-world
        ```

- Télécharger l'image alpine

    ??? note "Correction"
        ```shell
        docker image pull alpine
        ```

        Ici, on ne précise pas le tag. C'est donc le tag `latest` qui est téléchargé.

- Afficher les métadonnées de l'image téléchargée

    ??? note "Correction"
        ```shell
        docker image inspect alpine
        ```

        Dans les métadonnées on trouvera des informations utiles sur l'image :

        - date de création
        - type de processeur compatible
        - commande par défaut
        - ...



## Conteneur

- Démarrer un shell interactif de l'image alpine, observer le nom d'hôte apparent dans le conteneur

    ??? note "Correction"
        ```shell
        docker container run -i -t alpine sh
        ```

        Une fois dans le conteneur, on peut afficher le nom d'hôte du conteneur avec la commande `cat /etc/hostname`.

- dans un second terminal, observer les listes et stopper le conteneur.

    ??? note "Correction"
        ```shell
        docker container ls
        ```

        On peut voir le conteneur en état `Up`.

        ```shell
        docker container stop NAME
        ```

        ```shell
        docker container ls
        ```

        On peut voir le conteneur en état `Exited`. Dans le premier terminal, on peut voir que le shell est arrêté.

- relancer le conteneur et se rattacher.

    ??? note "Correction"

        ```shell
        docker container start NAME
        docker attach NAME
        ```

- stopper et supprimer le conteneur

    ??? note "Correction"

        - Soit dans une second terminal :
        ```shell
        docker container stop NAME 
        ```

        - Soit la commande `exit` dans le shell.

        Pour supprimer le conteneur, on utilise la commande `docker container rm NAME`.

Pour aller plus loin, chercher des informations sur les options `-d`, `-w`, `-h`, `--rm` et `--name` de la commande `docker container run` et tester ces options.

??? note "Correction"
    ```shell
    docker container run --help
    ```

## Volumes

### Volume hôte

- Démarrer un shell interactif de l'image alpine, en montant la racine de la machine hôte sur `/host` en mode lecture seule dans le conteneur. On peut vérifier que le contenu du dossier `/host` correspond bien au contenu de la machine hôte. Si on créé un fichier sur la machine hôte, on peut vérifier que le nouveau fichier apparaît bien dans un sous-dossier de `/host`. Stopper le conteneur.

    ??? note "Correction"
        ```shell
        docker container run -i -t -v /:/host:ro alpine sh
        ```

        Le fichier `/etc/hostname` contient bien le nom d'hôte du conteneur. Le fichier `/host/etc/hostname` contient le nom d'hôte de la machine hôte.


### Volume docker

- Démarrer un shell interactif avec un volume docker sur `/data`, créer un fichier avec la commande `touch /data/toto`. Lorsqu'on arrête et qu'on supprime le conteneur, le fichier n'est plus présent.

- Répéter cette opération avec un volume docker monté sur `/data`. Après le recréation du conteneur, le fichier créé est toujours présent.

- Affichez la liste des volumes et supprimer le volume.

    ??? note "Correction"
        ```shell
        docker volume ls
        docker volume rm NAME
        ```

        On ne peut supprimer un volume que si aucun conteneur ne l'utilise. Le message d'erreur indique l'identifiant du conteneur utilisant ce volume :
        
        ```
        $ docker volume rm monvolume
        Error response from daemon: remove monvolume: volume is in use - [a1bf0b7f7cb74abd283b3190af6efee0a9cc4d12bede45fe698b71c368b2f57a]
        $ docker container rm a1bf0b7f7cb7
        a1bf0b7f7cb7
        $ docker volume rm monvolume
        monvolume
        ```



## Réseau

### Mode réseau
- Affichez la liste des interfaces de votre machine hôte puis d'un conteneur avec la commande `ip a`
- Dans un conteneur avec l'option `--net host`, afficher les interfaces réseaux

??? note "Correction"
    L'option `--net host` branche le conteneur sur les interfaces réseaux de la machine hôte. **Il n'y a donc pas d'isolation réseau.**

### Exposition de ports

- Démarrez un conteneur basé sur l'image [https://hub.docker.com/r/containous/whoami/](https://hub.docker.com/r/containous/whoami/)

    ??? note "Correction"
        ```
        docker container run containous/whoami
        ```

- Trouvez l'IP du conteneur sur le réseau `bridge` en inspectant le conteneur. Dans un navigateur, consultez l'IP du conteneur `http://IP_CONTAINER`. Est-ce que ça fonctionne toujours en changeant cette adresse avec celle de la machine ?

    ??? note "Correction"
        ```shell
        docker container inspect NAME
        ```

        Les informations sur les réseaux du conteneur sont affichées sur la fin du JSON.
        
        L'IP du conteneur n'est accessible que depuis la machine hébergeant le conteneur. On ne peut pas y accéder depuis l'exterieur de la machine hôte avec son adresse IP. 

- Recréer un conteneur en ajoutant cette fois l'exposition du port 80 du conteneur sur le port 8080 de la machine et refaire les tests.

    ??? note "Correction"
        ```shell
        docker container run -p 8080:80 containous/whoami
        ```

        On peut toujours accéder au port 80 de l'IP du conteneur. On peut également accéder au port 8080 de la machine grâce à l'option `-p`.
